﻿using ModelDeDonnees.Entities;
using System.Data.Entity;
using System.Reflection;

namespace ModelDeDonnees
{
    public class MonContext : DbContext
    {
        public MonContext() : base("name=LeNomDeMaChaineDeConnextion")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.Configurations.Add(new EleveMapping());
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }

        public DbSet<Eleve> Eleves { get; set; }
        public DbSet<Classe> Classes { get; set; }
        public DbSet<Absence> Absences { get; set; }
        public DbSet<Notes> Notes { get; set; }

    }
}
