﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDeDonnees.Entities
{
    public class Eleve
    {
        public int Id { get; set; }

        public string nom { get; set; }
        public string prenom { get; set; }
        public DateTime dateNaissance { get; set; }
        public Classe Classe { get; set; }
        public int classeId { get; set; }
        public ICollection<Notes> Notes { get; set; }
        public ICollection<Absence> Absences { get; set; }
    }
}
