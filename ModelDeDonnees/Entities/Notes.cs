﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDeDonnees.Entities
{
    public class Notes
    {
        public int Id { get; set; }
        public string Matiere { get; set; }
        public DateTime DateNote { get; set; }
        public string Appreciation { get; set; }
        public int Note { get; set; }
        public Eleve Eleve { get; set; }
        public int EleveId { get; set; }
    }
}
