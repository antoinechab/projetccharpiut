﻿using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDeDonnees.Mapping
{
    public class EleveMapping : EntityTypeConfiguration<Eleve>
    {
        public EleveMapping()
        {
            ToTable("Eleve");
            HasKey(eleve => eleve.Id);

            Property(eleve => eleve.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(eleve => eleve.nom).IsRequired().HasMaxLength(255);
            Property(eleve => eleve.prenom).IsRequired().HasMaxLength(255);
            Property(eleve => eleve.dateNaissance).IsRequired();
            Property(eleve => eleve.classeId).IsRequired();

            HasMany(etudiant => etudiant.Notes).WithRequired(notes => notes.Eleve).HasForeignKey(note => note.EleveId);
            HasMany(etudiant => etudiant.Absences).WithRequired(absence => absence.Eleve).HasForeignKey(absence => absence.EleveId);
        }
    }
}
