﻿using ModelDeDonnees.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ModelDeDonnees.Mapping

{
    public class AbsenceMapping : EntityTypeConfiguration<Absence>
    {
        public AbsenceMapping()
        {
            ToTable("Absence");
            HasKey(absence => absence.Id);

            Property(absence => absence.DateAbsence).IsRequired();
            Property(absence => absence.Motif).IsRequired().HasMaxLength(255);
            Property(absence => absence.EleveId).IsRequired();

        }

    }
}
