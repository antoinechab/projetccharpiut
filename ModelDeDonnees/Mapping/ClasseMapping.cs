﻿using ModelDeDonnees.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ModelDeDonnees.Mapping
{
    public class ClasseMapping : EntityTypeConfiguration<Classe>
    {
        public ClasseMapping()
        {
            ToTable("Classes");
            HasKey(classes => classes.Id);

            Property(classes => classes.NomEtablissement).IsRequired().HasMaxLength(255);
            Property(classes => classes.Niveau).IsRequired().HasMaxLength(255);

            HasMany(classe => classe.Eleves).WithRequired(eleve => eleve.Classe).HasForeignKey(eleve => eleve.classeId);
        }
    }
}
