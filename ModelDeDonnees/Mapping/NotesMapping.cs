﻿using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDeDonnees.Mapping
{
    public class NotesMapping : EntityTypeConfiguration<Notes>
    {
        public NotesMapping()
        {
            ToTable("Notes");
            HasKey(notes => notes.Id);

            Property(notes => notes.Matiere).IsRequired().HasMaxLength(255);
            Property(notes => notes.DateNote).IsRequired();
            Property(notes => notes.Appreciation).IsRequired().HasMaxLength(255);
            Property(notes => notes.Note).IsRequired();
            Property(notes => notes.EleveId).IsRequired();

        }
    }
}
