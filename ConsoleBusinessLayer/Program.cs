﻿using BusinessLayer;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.EntitySql;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBusinessLayer
{
    class Program
    {
        static void Main(string[] args)
        {
            BusinessManager bm = BusinessManager.GetInstance();

            #region CRUD Eleve
            //GetAll(bm);
            //GetOne(bm); 
            //Push(bm);
            //Update(bm); 
            //Delete(bm);
            #endregion

            List<Eleve> eleves =  bm.GetBestStudent();

            foreach(Eleve e in eleves)
            {
                double moyenne = bm.GetAverageByEleveId(e.Id);
                Console.WriteLine("id: "+e.Id +" nom: "+e.nom+ " moyenne: "+ moyenne);
            }

        }

        static void GetAll(BusinessManager bm)
        {
            try
            {
                if (!bm.GetEleves().Any())
                {
                    if (!bm.GetClasses().Any())
                    {
                        try
                        {
                            bm.CreateClasse("monnet", "lycée");
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création de classe dans la console du business layer: " + e);
                        }
                    }
                    Classe idClasse = bm.GetClasseById(1);
                    bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);

                }
                List<Eleve> eleves = bm.GetEleves();
                eleves.ForEach(delegate (Eleve eleve)
                {
                    Console.WriteLine("id: " + eleve.Id + " " + "nom: " + eleve.nom + " " + "prénom: " + eleve.prenom);
                });
            }
            catch (Exception e)
            {
                throw new Exception("erreur de récupération des élèves dans la console du business layer: " + e);
            }
        }

        static void GetOne(BusinessManager bm)
        {
            try
            {
                if (!bm.GetEleves().Any())
                {
                    try
                    {
                        if (!bm.GetClasses().Any())
                        {
                            try
                            {
                                bm.CreateClasse("monnet", "lycée");
                            }
                            catch (Exception e)
                            {
                                throw new Exception("erreur de création de classe dans la console du business layer: " + e);
                            }
                        }
                        Classe idClasse = bm.GetClasseById(bm.GetClasses().First().Id);
                        bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création d'élève dans la console du business layer: " + e);
                    }
                }
                Eleve eleve = bm.GetEleveById(bm.GetEleves().Last().Id);
                Console.WriteLine("id: " + eleve.Id + " " + "nom: " + eleve.nom + " " + "prénom: " + eleve.prenom);

            }
            catch (Exception e)
            {
                throw new Exception("erreur de recupération d'élève dans la console du business layer: " + e);
            }
        }

        static void Push(BusinessManager bm)
        {
            try
            {
                if (!bm.GetClasses().Any())
                {
                    try
                    {
                        bm.CreateClasse("monnet", "lycée");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création de classe dans la console du business layer: " + e);
                    }
                }
                Classe idClasse = bm.GetClasseById(1);
                bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                List<Eleve> lesEleves = bm.GetEleves();
                lesEleves.ForEach(delegate (Eleve eleve)
                {
                    Console.WriteLine("id: " + eleve.Id + " " + "nom: " + eleve.nom + " " + "prénom: " + eleve.prenom);
                });
            }
            catch (Exception e)
            {
                throw new Exception("erreur de création d'élève dans la console du business layer: " + e);
            }
        }

        static void Update(BusinessManager bm)
        {
            try
            {
                if (!bm.GetEleves().Any())
                {
                    if (!bm.GetClasses().Any())
                    {
                        try
                        {
                            bm.CreateClasse("monnet", "lycée");
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création de classe dans la console du business layer: " + e);
                        }
                    }
                    Classe idClasse = bm.GetClasseById(bm.GetClasses().First().Id);
                    bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                }

                bm.UpdateEleveById(bm.GetEleves().Last().Id, "chabreuil", "antoine", DateTime.Now, bm.GetClasses().First().Id);
                Eleve eleve = bm.GetEleveById(bm.GetEleves().Last().Id);
                Console.WriteLine("id: " + eleve.Id + " " + "nom: " + eleve.nom + " " + "prénom: " + eleve.prenom);

            }
            catch (Exception e)
            {
                throw new Exception("erreur de mise a jour d'un élève dans la console du business layer: " + e);
            }
        }

        static void Delete(BusinessManager bm)
        {
            try
            {
                if (!bm.GetEleves().Any())
                {
                    if (!bm.GetClasses().Any())
                    {
                        try
                        {
                            bm.CreateClasse("monnet", "lycée");
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création de classe dans la console du business layer: " + e);
                        }
                    }
                    Classe idClasse = bm.GetClasseById(bm.GetClasses().First().Id);
                    bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                }
                bm.DeleteEleveById(bm.GetEleves().Last().Id);
                Console.WriteLine("eleve supprimé");
            }
            catch (Exception e)
            {
                throw new Exception("erreur de suppression d'un élève dans la console du business layer: " + e);
            }
        }
    }
}
