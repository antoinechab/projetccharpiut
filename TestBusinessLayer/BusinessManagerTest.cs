﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModelDeDonnees.Entities;

namespace TestBusinessLayer
{
    [TestClass]
    public class BusinessManagerTest
    {
        private BusinessManager _bm;

        public BusinessManagerTest()
        {
            _bm = BusinessManager.GetInstance();
        }

        #region Eleves
        [TestMethod]
        public void TestGetAllEleve()
        {
            try
            {
                if (!_bm.GetEleves().Any())
                {
                    if (!_bm.GetClasses().Any())
                    {
                        try
                        {
                            _bm.CreateClasse("monnet", "lycée");
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création de classe dans le test TestGetAllEleve: " + e);
                        }
                    }
                    try
                    {
                        Classe idClasse = _bm.GetClasses().Last();
                        _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                    }
                    catch(Exception e)
                    {
                        throw new Exception("erreur de création d'élève dans le test TestGetAllEleve: " + e);
                    }

                }
                List<Eleve> eleves = _bm.GetEleves();
            }
            catch (Exception e)
            {
                throw new Exception("erreur de récupération des élèves dans le test TestGetAllEleve: " + e);
            }
        }

        [TestMethod]
        public void TestGetElevesByClasseId()
        {
            try
            {
                if (!_bm.GetEleves().Any())
                {
                    if (!_bm.GetClasses().Any())
                    {
                        try
                        {
                            _bm.CreateClasse("monnet", "lycée");
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création de classe dans le test TestGetElevesByClasseId: " + e);
                        }
                    }
                    try
                    {
                        Classe idClasse = _bm.GetClasses().Last();
                        _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création d'élève dans le test TestGetElevesByClasseId: " + e);
                    }

                }
                List<Eleve> eleves = _bm.GetElevesByClasseId(_bm.GetClasses().First().Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de récupération des élèves dans le test TestGetElevesByClasseId: " + e);
            }
        }

        [TestMethod]
        public void TestGetOneByIdEleve()
        {
            try
            {
                if (!_bm.GetEleves().Any())
                {
                    try
                    {
                        if (!_bm.GetClasses().Any())
                        {
                            try
                            {
                                _bm.CreateClasse("monnet", "lycée");
                            }
                            catch (Exception e)
                            {
                                throw new Exception("erreur de création de classe dans le test TestGetOneByIdEleve: " + e);
                            }
                        }
                        try
                        {
                            Classe idClasse = _bm.GetClasseById(_bm.GetClasses().First().Id);
                            _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création d'élève dans le test TestGetOneByIdEleve: " + e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création d'élève dans le test TestGetOneByIdEleve: " + e);
                    }
                }
                Eleve eleve = _bm.GetEleveById(_bm.GetEleves().Last().Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de recupération d'élève dans le test TestGetOneByIdEleve: " + e);
            }
        }

        [TestMethod]
        public void TestPushEleve()
        {
            try
            {
                if (!_bm.GetClasses().Any())
                {
                    try
                    {
                        _bm.CreateClasse("monnet", "lycée");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création de classe dans le test TestPushEleve: " + e);
                    }
                }
                try
                {
                    Classe idClasse = _bm.GetClasses().Last();
                    _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                }
                catch (Exception e)
                {
                    throw new Exception("erreur de création d'élève dans le test TestPushEleve: " + e);
                }
            }
            catch (Exception e)
            {
                throw new Exception("erreur de création d'élève dans le test TestPushEleve: " + e);
            }
        }

        [TestMethod]
        public void TestUpdateEleve()
        {
            try
            {
                if (!_bm.GetEleves().Any())
                {
                    if (!_bm.GetClasses().Any())
                    {
                        try
                        {
                            _bm.CreateClasse("monnet", "lycée");
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création de classe dans le test TestUpdateEleve: " + e);
                        }
                    }
                    try
                    {
                        Classe idClasse = _bm.GetClasseById(_bm.GetClasses().First().Id);
                        _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création d'élève dans le test TestUpdateEleve: " + e);
                    }
                }
                _bm.UpdateEleveById(_bm.GetEleves().Last().Id, "chabreuil", "antoine", DateTime.Now, _bm.GetClasses().First().Id);
                Eleve eleve = _bm.GetEleveById(_bm.GetEleves().Last().Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de mise a jour d'un élève dans le test TestUpdateEleve: " + e);
            }
        }

        [TestMethod]
        public void TestDeleteEleve()
        {
            try
            {
                if (!_bm.GetEleves().Any())
                {
                    if (!_bm.GetClasses().Any())
                    {
                        try
                        {
                            _bm.CreateClasse("monnet", "lycée");
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création de classe dans le test TestDeleteEleve: " + e);
                        }
                    }
                    try
                    {
                        Classe idClasse = _bm.GetClasseById(_bm.GetClasses().First().Id);
                        _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création d'élève dans le test TestDeleteEleve: " + e);
                    }
                }
                _bm.DeleteEleveById(_bm.GetEleves().Last().Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de suppression d'un élève dans le test TestDeleteEleve: " + e);
            }
        }
        #endregion

        #region Classe
        [TestMethod]
        public void TestGetAllClasse()
        {
            try
            {
                if (!_bm.GetClasses().Any())
                {
                    try
                    {
                        _bm.CreateClasse("Monnet", "lycée");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création de classe dans le test TestGetAllClasse: " + e);
                    }

                }
                List<Classe> classes = _bm.GetClasses();
            }
            catch (Exception e)
            {
                throw new Exception("erreur de récupération des classes dans TestGetAllClasse: " + e);
            }
        }

        [TestMethod]
        public void TestGetOneByIdClasse()
        {
            try
            {
                if (!_bm.GetClasses().Any())
                {
                    try
                    {
                        _bm.CreateClasse("Monnet", "lycée");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création d'une classe dans TestGetOneByIdClasse: " + e);
                    }
                }
                Classe classe = _bm.GetClasseById(_bm.GetClasses().Last().Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de recupération des classes dans TestGetOneByIdClasse: " + e);
            }
        }

        [TestMethod]
        public void TestPushClasse()
        {
            try
            {
                _bm.CreateClasse("Monnet", "lycée");
            }
            catch (Exception e)
            {
                throw new Exception("erreur de création de classe dans TestPushClasse: " + e);
            }
        }

        /*
        [TestMethod]
        public void TestUpdateClasse()
        {
            Assert.AreEqual(1, 1);
        }

        [TestMethod]
        public void TestDeleteClasse()
        {
            Assert.AreEqual(1, 1);
        }
        */
        #endregion

        #region Absence
        [TestMethod]
        public void TestGetAllAbsence()
        {
            try
            {
                if (!_bm.GetAbsences().Any())
                {
                    if (!_bm.GetEleves().Any())
                    {
                        if (!_bm.GetClasses().Any())
                        {
                            try
                            {
                                _bm.CreateClasse("monnet", "lycée");
                            }
                            catch (Exception e)
                            {
                                throw new Exception("erreur de création de classe dans TestGetAllAbsence: " + e);
                            }
                        }
                        try
                        {
                            Classe idClasse = _bm.GetClasseById(_bm.GetClasses().Last().Id);
                            _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création d'élève dans TestGetAllAbsence: " + e);
                        }
                    }
                    try
                    {
                        _bm.CreateAbscence(DateTime.Now, "retard bus", _bm.GetEleves().Last().Id);
                    }
                    catch(Exception e)
                    {
                        throw new Exception("erreur de création d'abscence dans TestGetAllAbsence: " + e);
                    }
                }
                List<Absence> absences = _bm.GetAbsences().ToList();
            }
            catch (Exception e)
            {
                throw new Exception("erreur de récupération d'abscences dans TestGetAllAbsence: " + e);
            }
        }

        [TestMethod]
        public void TestGetOneByIdAbsence()
        {
            try
            {
                if (!_bm.GetAbsences().Any())
                {
                    if (!_bm.GetEleves().Any())
                    {
                        if (!_bm.GetClasses().Any())
                        {
                            try
                            {
                                _bm.CreateClasse("monnet", "lycée");
                            }
                            catch (Exception e)
                            {
                                throw new Exception("erreur de création de classe dans TestGetOneByIdAbsence: " + e);
                            }
                        }
                        try
                        {
                            Classe idClasse = _bm.GetClasseById(_bm.GetClasses().Last().Id);
                            _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création d'élève dans TestGetOneByIdAbsence: " + e);
                        }
                    }
                    try
                    {
                        _bm.CreateAbscence(DateTime.Now, "retard bus", _bm.GetEleves().Last().Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création d'absence dans TestGetOneByIdAbsence: " + e);
                    }
                }
                int id = _bm.GetEleves().ToList().Last().Id;
                Absence absence = _bm.GetAbsences().FirstOrDefault(a => a.EleveId == id);
                Console.WriteLine("abscence affiché");
            }
            catch (Exception e)
            {
                throw new Exception("erreur de récupération d'absence dans TestGetOneByIdAbsence: " + e);
            }
        }

        [TestMethod]
        public void TestPushAbsence()
        {
            if (!_bm.GetEleves().Any())
            {
                if (!_bm.GetClasses().Any())
                {
                    try
                    {
                        _bm.CreateClasse("monnet", "lycée");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création de classe dans TestPushAbsence: " + e);
                    }
                }
                try
                {
                    Classe idClasse = _bm.GetClasseById(_bm.GetClasses().Last().Id);
                    _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                }
                catch (Exception e)
                {
                    throw new Exception("erreur de création d'élève dans TestPushAbsence: " + e);
                }
            }
            try
            {
                _bm.CreateAbscence(DateTime.Now, "retard bus", _bm.GetEleves().Last().Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de création d'absence dans TestPushAbsence: " + e);
            }
        }

        [TestMethod]
        public void TestUpdateAbsence()
        {
            try
            {
                if (!_bm.GetAbsences().Any())
                {
                    if (!_bm.GetEleves().Any())
                    {
                        if (!_bm.GetClasses().Any())
                        {
                            try
                            {
                                _bm.CreateClasse("monnet", "lycée");
                            }
                            catch (Exception e)
                            {
                                throw new Exception("erreur de création de classe dans TestUpdateAbsence: " + e);
                            }
                        }
                        try
                        {
                            Classe idClasse = _bm.GetClasseById(_bm.GetClasses().Last().Id);
                            _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création d'élève dans TestUpdateAbsence: " + e);
                        }
                    }
                    try
                    {
                        _bm.CreateAbscence(DateTime.Now, "retard bus", _bm.GetEleves().Last().Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création d'absence dans TestUpdateAbsence: " + e);
                    }
                }
                int id = _bm.GetAbsences().ToList().Last().Id;
                _bm.UpdateAbscenceById(id, DateTime.Now, "retard bus", _bm.GetEleves().Last().Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de mise a jour d'abscence dans TestUpdateAbsence: " + e);
            }
        }

        [TestMethod]
        public void TestDeleteAbsence()
        {
            try
            {
                if (!_bm.GetAbsences().Any())
                {
                    if (!_bm.GetEleves().Any())
                    {
                        if (!_bm.GetClasses().Any())
                        {
                            try
                            {
                                _bm.CreateClasse("monnet", "lycée");
                            }
                            catch (Exception e)
                            {
                                throw new Exception("erreur de création de classe dans TestDeleteAbsence: " + e);
                            }
                        }
                        try
                        {
                            Classe idClasse = _bm.GetClasseById(_bm.GetClasses().Last().Id);
                            _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création d'élève dans TestDeleteAbsence: " + e);
                        }
                    }
                    try
                    {
                        _bm.CreateAbscence(DateTime.Now, "retard bus", _bm.GetEleves().Last().Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création d'abscence dans TestDeleteAbsence: " + e);
                    }
                }
                Absence absence = _bm.GetAbsences().ToList().Last();
                _bm.DeleteAbscenceById(absence.Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de suppression d'absence dans TestDeleteAbsence: " + e);
            }
        }
        #endregion

        #region Note
        [TestMethod]
        public void TestGetAllNote()
        {
            try
            {
                if (!_bm.GetNotes().Any())
                {
                    if (!_bm.GetEleves().Any())
                    {
                        if (!_bm.GetClasses().Any())
                        {
                            try
                            {
                                _bm.CreateClasse("monnet", "lycée");
                            }
                            catch (Exception e)
                            {
                                throw new Exception("erreur de création de classe dans TestGetAllNote: " + e);
                            }
                        }
                        try
                        {
                            Classe idClasse = _bm.GetClasseById(_bm.GetClasses().Last().Id);
                            _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création d'élève dans TestGetAllNote: " + e);
                        }
                    }
                    try
                    {
                        _bm.CreateNote("anglais", DateTime.Now, "pas ouf", 15 , _bm.GetEleves().Last().Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création de note dans TestGetAllNote: " + e);
                    }
                }
                List<Notes> notes = _bm.GetNotes().ToList();
                Console.WriteLine("notes listée");
            }
            catch (Exception e)
            {
                throw new Exception("erreur de récupération des notes dans TestGetAllNote: " + e);
            }
        }

        [TestMethod]
        public void TestGetOneByIdNote()
        {
            try
            {
                if (!_bm.GetNotes().Any())
                {
                    if (!_bm.GetEleves().Any())
                    {
                        if (!_bm.GetClasses().Any())
                        {
                            try
                            {
                                _bm.CreateClasse("monnet", "lycée");
                            }
                            catch (Exception e)
                            {
                                throw new Exception("erreur de création de classe dans TestGetOneByIdNote: " + e);
                            }
                        }
                        try
                        {
                            Classe idClasse = _bm.GetClasseById(_bm.GetClasses().Last().Id);
                            _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création d'élève dans TestGetOneByIdNote: " + e);
                        }
                    }
                    try
                    {
                        _bm.CreateNote("anglais", DateTime.Now, "pas ouf", 15, _bm.GetEleves().Last().Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création de note dans TestGetOneByIdNote: " + e);
                    }
                }
                int id = _bm.GetEleves().ToList().Last().Id;
                Notes note = _bm.GetNotes().FirstOrDefault(a => a.EleveId == id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de récupération des notes dans TestGetOneByIdNote: " + e);
            }
        }

        [TestMethod]
        public void TestPushNote()
        {
            if (!_bm.GetEleves().Any())
            {
                if (!_bm.GetClasses().Any())
                {
                    try
                    {
                        _bm.CreateClasse("monnet", "lycée");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création de classe dans TestPushNote: " + e);
                    }
                }
                try
                {
                    Classe idClasse = _bm.GetClasseById(_bm.GetClasses().Last().Id);
                    _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                }
                catch (Exception e)
                {
                    throw new Exception("erreur de création d'élève dans TestPushNote: " + e);
                }
            }
            try
            {
                _bm.CreateNote("anglais", DateTime.Now, "pas ouf", 15, _bm.GetEleves().Last().Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de création de note dans TestPushNote: " + e);
            }
        }

        [TestMethod]
        public void TestUpdateNote()
        {
            try
            {
                if (!_bm.GetNotes().Any())
                {
                    if (!_bm.GetEleves().Any())
                    {
                        if (!_bm.GetClasses().Any())
                        {
                            try
                            {
                                _bm.CreateClasse("monnet", "lycée");
                            }
                            catch (Exception e)
                            {
                                throw new Exception("erreur de création de classe dans TestUpdateNote: " + e);
                            }
                        }
                        try
                        {
                            Classe idClasse = _bm.GetClasseById(_bm.GetClasses().Last().Id);
                            _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création d'élève dans TestUpdateNote: " + e);
                        }
                    }
                    try
                    {
                        _bm.CreateNote("anglais", DateTime.Now, "pas ouf", 15, _bm.GetEleves().Last().Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création de note dans TestUpdateNote: " + e);
                    }
                }
                int id = _bm.GetNotes().ToList().Last().Id;
                _bm.UpdateNoteById(id, "anglais", DateTime.Now, "pas ouf", 15, _bm.GetEleves().Last().Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de mise a jour de note dans TestUpdateNote: " + e);
            }
        }

        [TestMethod]
        public void TestDeleteNote()
        {
            try
            {
                if (!_bm.GetNotes().Any())
                {
                    if (!_bm.GetEleves().Any())
                    {
                        if (!_bm.GetClasses().Any())
                        {
                            try
                            {
                                _bm.CreateClasse("monnet", "lycée");
                            }
                            catch (Exception e)
                            {
                                throw new Exception("erreur de création de classe dans TestUpdateNote: " + e);
                            }
                        }
                        try
                        {
                            Classe idClasse = _bm.GetClasseById(_bm.GetClasses().Last().Id);
                            _bm.CreateEleve("chabreuil", "antoine", DateTime.Now, idClasse.Id);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("erreur de création d'élève dans TestUpdateNote: " + e);
                        }
                    }
                    try
                    {
                        _bm.CreateNote("anglais", DateTime.Now, "pas ouf", 15, _bm.GetEleves().Last().Id);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("erreur de création de note dans TestUpdateNote: " + e);
                    }
                }
                Notes notes = _bm.GetNotes().ToList().Last();
                _bm.DeleteNoteById(notes.Id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur de suppression de note dans TestUpdateNote: " + e);
            }
        }
        #endregion

    }
}
