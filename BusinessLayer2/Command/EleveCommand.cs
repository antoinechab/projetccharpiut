﻿using ModelDeDonnees;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer2.Command
{
    internal class EleveCommand
    {
        private MonContext _contexte;
        public EleveCommand(MonContext contexte)
        {
            _contexte = contexte;
        }

        public void CreateEleve(string nom, string prenom, DateTime dateNaissance , int classeId)
        {
            var eleve = new Eleve();

            if (nom != "" && nom != null)
            {
                eleve.nom = nom;
            }
            if (prenom != "" && prenom != null)
            {
                eleve.prenom = prenom;
            }
            if (dateNaissance != null && dateNaissance != DateTime.MinValue)
            {
                eleve.dateNaissance = dateNaissance;
            }
            if (classeId >= 0)
            {
                eleve.classeId = classeId;
            }

            _contexte.Eleves.Add(eleve);
            _contexte.SaveChanges();
        }

        public void DeleteEleveById(int id)
        {
            var eleve = _contexte.Eleves.FirstOrDefault(e => e.Id == id);
            _contexte.Eleves.Remove(eleve);

            _contexte.SaveChanges();
        }

        public void UpdateEleveById(int id, string nom, string prenom, DateTime? dateNaissance = null, int? classeId = null)
        {
            var eleve = _contexte.Eleves.FirstOrDefault(e => e.Id == id);

            //todo voir si il est possible de définire les type comme type or null dans les param
            if (nom != "" && nom != eleve.nom)
            {
                eleve.nom = nom;
            }
            if (prenom != "" && prenom != eleve.prenom)
            {
                eleve.prenom = prenom;
            }
            if (dateNaissance.HasValue && dateNaissance != eleve.dateNaissance)
            {
                eleve.dateNaissance = (DateTime) dateNaissance;
            }
            if (classeId >= 0 && classeId != eleve.classeId)
            {
                eleve.classeId = (int) classeId;
            }

            _contexte.SaveChanges();
        }
    }
}
