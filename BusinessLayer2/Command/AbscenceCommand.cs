﻿using ModelDeDonnees;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer2.Command
{
    internal class AbscenceCommand
    {
        private MonContext _contexte;
        public AbscenceCommand(MonContext contexte)
        {
            _contexte = contexte;
        }

        public void CreateAbscence(DateTime DateAbsence, string Motif, int eleveId)
        {
            var abscence = new Absence();

            if (DateAbsence != null && DateAbsence != DateTime.MinValue)
            {
                abscence.DateAbsence = DateAbsence;
            }
            if (Motif != "" && Motif != null)
            {
                abscence.Motif = Motif;
            }
            if (eleveId >= 0)
            {
                abscence.EleveId = eleveId;
            }

            _contexte.Absences.Add(abscence);
            _contexte.SaveChanges();
        }

        public void DeleteAbscenceById(int id)
        {
            var abscence = _contexte.Absences.FirstOrDefault(e => e.Id == id);
            _contexte.Absences.Remove(abscence);

            _contexte.SaveChanges();
        }

        public void UpdateAbscenceById(int id, DateTime DateAbsence, string Motif, int eleveId)
        {
            var abscence = _contexte.Absences.FirstOrDefault(e => e.Id == id);

            if (DateAbsence != null && DateAbsence != DateTime.MinValue)
            {
                abscence.DateAbsence = DateAbsence;
            }
            if (Motif != "" && Motif != null)
            {
                abscence.Motif = Motif;
            }
            if (eleveId >= 0)
            {
                abscence.EleveId = eleveId;
            }

            _contexte.SaveChanges();
        }
    }
}
