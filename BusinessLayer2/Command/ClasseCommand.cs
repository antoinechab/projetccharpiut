﻿using ModelDeDonnees;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer2.Command
{
    internal class ClasseCommand
    {
        private MonContext _contexte;
        public ClasseCommand(MonContext contexte)
        {
            _contexte = contexte;
        }


        public void CreateClasse(string nomEtablissement, string niveau)
        {
            var classe = new Classe();

            if (nomEtablissement != "" && nomEtablissement != null)
            {
                classe.NomEtablissement = nomEtablissement;
            }
            if (niveau != "" && niveau != null)
            {
                classe.Niveau = niveau;
            }

            _contexte.Classes.Add(classe);
            _contexte.SaveChanges();
        }
        
        public void UpdateClasseById(int id, string nomEtablissement, string niveau)
        {
            var classe = _contexte.Classes.FirstOrDefault(c => c.Id == id);

            if (nomEtablissement != "" && nomEtablissement != null)
            {
                classe.NomEtablissement = nomEtablissement;
            }
            if (niveau != "" && niveau != null)
            {
                classe.Niveau = niveau;
            }

            _contexte.SaveChanges();
        }

    }
}
