﻿using ModelDeDonnees;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer2.Command
{
    internal class NoteCommand
    {
        private MonContext _contexte;
        public NoteCommand(MonContext contexte)
        {
            _contexte = contexte;
        }

        public void CreateNote(string matiere, DateTime dateNote, string appreciation, int noteVal, int eleveId)
        {
            var note = new Notes();

            if (matiere != "" && matiere != null)
            {
                note.Matiere = matiere;
            }
            if (dateNote != null && dateNote != DateTime.MinValue)
            {
                note.DateNote = dateNote;
            }
            if (appreciation != "" && appreciation != null)
            {
                note.Appreciation = appreciation;
            }
            if (noteVal >= 0)
            {
                note.Note = noteVal;
            }
            if (eleveId >= 0)
            {
                note.EleveId = eleveId;
            }

            _contexte.Notes.Add(note);
            _contexte.SaveChanges();
        }

        public void DeleteNoteById(int id)
        {
            var note = _contexte.Notes.FirstOrDefault(e => e.Id == id);
            _contexte.Notes.Remove(note);

            _contexte.SaveChanges();
        }

        public void UpdateNoteById(int id, string matiere, DateTime dateNote, string appreciation, int noteVal, int eleveId)
        {
            var note = _contexte.Notes.FirstOrDefault(e => e.Id == id);

            if (matiere != "" && matiere != null)
            {
                note.Matiere = matiere;
            }
            if (dateNote != null && dateNote != DateTime.MinValue)
            {
                note.DateNote = dateNote;
            }
            if (appreciation != "" && appreciation != null)
            {
                note.Appreciation = appreciation;
            }
            if (noteVal >= 0)
            {
                note.Note = noteVal;
            }
            if (eleveId >= 0)
            {
                note.EleveId = eleveId;
            }

            _contexte.SaveChanges();
        }
    }
}
