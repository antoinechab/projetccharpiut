﻿using BusinessLayer.Query;
using BusinessLayer2.Command;
using BusinessLayer2.Query;
using ModelDeDonnees;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class BusinessManager
    {
        #region ossature
        public static BusinessManager _instance;
        private MonContext _monContexte; 

        private BusinessManager() 
        {
            _monContexte = new MonContext();
        }

        public static BusinessManager GetInstance()
        {
            if (_instance != null)
            {
                return _instance;
            }

            _instance = new BusinessManager();
            return _instance;
        }
        #endregion

        #region Eleve query/command
        #region query
        public List<Eleve> GetEleves()
        {
            EleveQuery query = new EleveQuery(_monContexte);
            try
            {
                var res = query.GetAll();
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetEleves: " + e);
            }
           
        }

        public Eleve GetEleveById(int id)
        {
            EleveQuery query = new EleveQuery(_monContexte);

            try
            {
                var res = query.GetEleveById(id);
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetEleveById: " + e);
            }
            
        }

        public List<Eleve> GetElevesByClasseId(int id)
        {
            EleveQuery query = new EleveQuery(_monContexte);

            try
            {
                var res = query.GetElevesByClasseId(id);
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetElevesByClasseId: " + e);
            }

        }

        public List<Eleve> GetBestStudent()
        {
            EleveQuery query = new EleveQuery(_monContexte);

            try
            {
                var res = query.GetBestStudent();
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetBestStudent: " + e);
            }

        }

        #endregion
        #region command

        public void CreateEleve(string nom, string prenom, DateTime dateNaissance, int classeId)
        {
            EleveCommand query = new EleveCommand(_monContexte);

            try
            {
                query.CreateEleve(nom,prenom,dateNaissance,classeId);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans CreateEleve: " + e);
            }

        }

        public void  DeleteEleveById(int id)
        {
            EleveCommand query = new EleveCommand(_monContexte);

            try
            {
                query.DeleteEleveById(id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetEleveById: " + e);
            }

        }

        public void UpdateEleveById(int id, string nom, string prenom, DateTime dateNaissance, int classeId)
        {
            EleveCommand query = new EleveCommand(_monContexte);

            try
            {
                query.UpdateEleveById( id,  nom,  prenom,  dateNaissance,  classeId);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetEleveById: " + e);
            }

        }
        #endregion
        #endregion

        #region classe query / command
        #region query
        public List<Classe> GetClasses()
        {
            ClasseQuery query = new ClasseQuery(_monContexte);
            try
            {
                var res = query.GetAll();
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetClasses: " + e);
            }

        }

        public Classe GetClasseById(int id)
        {
            ClasseQuery query = new ClasseQuery(_monContexte);

            try
            {
                var res = query.GetClasseById(id);
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetClasseById: " + e);
            }

        }
        #endregion
        #region command

        public void CreateClasse(string nomEtablissement, string niveau)
        {
            ClasseCommand query = new ClasseCommand(_monContexte);

            try
            {
                query.CreateClasse(nomEtablissement, niveau);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans CreateClasse: " + e);
            }

        }

        public void UpdateClasseById(int id, string nomEtablissement, string niveau)
        {
            ClasseCommand query = new ClasseCommand(_monContexte);

            try
            {
                query.UpdateClasseById(id, nomEtablissement, niveau);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans UpdateClasseById: " + e);
            }

        }

        #endregion
        #endregion

        #region Notes query / command
        #region Query

        public List<Notes> GetNotes()
        {
            NoteQuery query = new NoteQuery(_monContexte);
            try
            {
                var res = query.GetNotes();
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetNotes: " + e);
            }
        }

        public Notes GetNoteById(int id)
        {
            NoteQuery query = new NoteQuery(_monContexte);
            try
            {
                var res = query.GetNoteById(id);
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetNoteById: " + e);
            }
        }

        public List<Notes> GetNotesByEleveId(int id)
        {
            NoteQuery query = new NoteQuery(_monContexte);
            try
            {
                var res = query.GetNotesByEleveId(id);
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetNotesByEleveId: " + e);
            }
        }

        public double GetAverageByEleveId(int id)
        {
            NoteQuery query = new NoteQuery(_monContexte);
            try
            {
                var res = query.GetAverageByEleveId(id);
                return res;
            }
            catch(Exception e)
            {
                throw new Exception("erreur dans GetAverageByEleveId: " + e);
            }
        }
        #endregion
        #region Command

        public void CreateNote(string matiere, DateTime dateNote, string appreciation, int noteVal, int eleveId)
        {
            NoteCommand query = new NoteCommand(_monContexte);

            try
            {
                query.CreateNote(matiere, dateNote, appreciation, noteVal, eleveId);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans CreateNote: " + e);
            }

        }

        public void DeleteNoteById(int id)
        {
            NoteCommand query = new NoteCommand(_monContexte);

            try
            {
                query.DeleteNoteById(id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans DeleteNoteById: " + e);
            }

        }

        public void UpdateNoteById(int id, string matiere, DateTime dateNote, string appreciation, int noteVal, int eleveId)
        {
            NoteCommand query = new NoteCommand(_monContexte);

            try
            {
                query.UpdateNoteById(id, matiere, dateNote, appreciation, noteVal, eleveId);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans UpdateNoteById: " + e);
            }

        }

        #endregion

        #endregion

        #region Abscence query / command
        #region Query

        public List<Absence> GetAbsences()
        {
            AbscenceQuery query = new AbscenceQuery(_monContexte);
            try
            {
                var res = query.GetAbsences();
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetAbsences: " + e);
            }
        }

        public Absence GetAbscenceById(int id)
        {
            AbscenceQuery query = new AbscenceQuery(_monContexte);
            try
            {
                var res = query.GetAbscenceById(id);
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetAbscenceById: " + e);
            }
        }

        public List<Absence> GetAbscenceByEleveId(int id)
        {
            AbscenceQuery query = new AbscenceQuery(_monContexte);
            try
            {
                var res = query.GetAbscenceByEleveId(id);
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetAbscenceByEleveId: " + e);
            }
        }

        public int GetTotalAbscenceByEleveId(int id)
        {
            AbscenceQuery query = new AbscenceQuery(_monContexte);
            try
            {
                var res = query.GetTotalAbscenceByEleveId(id);
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetTotalAbscenceByEleveId: " + e);
            }
        }

        public List<Absence> GetLastAbsence()
        {
            AbscenceQuery query = new AbscenceQuery(_monContexte);
            try
            {
                var res = query.GetLastAbsence();
                return res;
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans GetAbsences: " + e);
            }
        }

        #endregion
        #region Command

        public void CreateAbscence(DateTime DateAbsence, string Motif, int eleveId)
        {
            AbscenceCommand query = new AbscenceCommand(_monContexte);

            try
            {
                query.CreateAbscence(DateAbsence, Motif, eleveId);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans CreateAbscence: " + e);
            }

        }

        public void DeleteAbscenceById(int id)
        {
            AbscenceCommand query = new AbscenceCommand(_monContexte);

            try
            {
                query.DeleteAbscenceById(id);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans DeleteAbscenceById: " + e);
            }

        }

        public void UpdateAbscenceById(int id, DateTime DateAbsence, string Motif, int eleveId)
        {
            AbscenceCommand query = new AbscenceCommand(_monContexte);

            try
            {
                query.UpdateAbscenceById(id, DateAbsence, Motif, eleveId);
            }
            catch (Exception e)
            {
                throw new Exception("erreur dans UpdateAbscenceById: " + e);
            }

        }

        #endregion
        #endregion
    }
}
