﻿using ModelDeDonnees;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer2.Query
{
    internal class ClasseQuery
    {
        private MonContext _contexte;
        public ClasseQuery(MonContext contexte)
        {
            _contexte = contexte;
        }

        public List<Classe> GetAll()
        {
            return _contexte.Classes.ToList();
        }

        public Classe GetClasseById(int id)
        {
            return _contexte.Classes.FirstOrDefault(e => e.Id == id);
        }
    }
}
