﻿using ModelDeDonnees;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace BusinessLayer.Query
{
    internal class EleveQuery
    {
        private MonContext _contexte;
        public EleveQuery(MonContext contexte)
        {
            _contexte = contexte;
        }

        public List<Eleve> GetAll()
        {
            return _contexte.Eleves.ToList();
        }

        public Eleve GetEleveById(int id)
        {
            return _contexte.Eleves.FirstOrDefault(e => e.Id == id);
        }
        
        public List<Eleve> GetElevesByClasseId(int id)
        {
            return _contexte.Eleves.ToList().FindAll(e => e.classeId == id);
        }
        
        public List<Eleve> GetBestStudent()
        {
            NoteQuery query = new NoteQuery(_contexte);
            List<Eleve> bestEleves = new List<Eleve>();

            List<Eleve> eleves = _contexte.Eleves.ToList();

            Dictionary<Eleve, double> arrayNoteEleve = new Dictionary<Eleve, double>();

            int i =  0;
            foreach(Eleve eleve in eleves)
            {
                double moyenne = query.GetAverageByEleveId(eleve.Id);
                arrayNoteEleve.Add(eleve,moyenne);
                i++;
            }

            return arrayNoteEleve.OrderByDescending(m => m.Value).Take(5).Select(m => m.Key).ToList();
        }
    }
}
