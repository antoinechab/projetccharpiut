﻿
using ModelDeDonnees;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Query
{
    internal class NoteQuery
    {
        private MonContext _contexte;
        public NoteQuery(MonContext contexte)
        {
            _contexte = contexte;
        }

        public List<Notes> GetNotes()
        {
            return _contexte.Notes.ToList();
        }

        public Notes GetNoteById(int id)
        {
            return _contexte.Notes.FirstOrDefault(e => e.Id == id);
        }

        public List<Notes> GetNotesByEleveId(int id)
        {
           return _contexte.Notes.ToList().FindAll(n => n.EleveId == id);
        }

        public double GetAverageByEleveId(int id)
        {
            double ret = 0;
            try
            {
                List<double> listNotes = new List<double>();
                foreach (Notes note in _contexte.Notes.ToList().FindAll(a => a.EleveId == id))
                {
                    listNotes.Add(note.Note);
                }
                ret = listNotes.Average();
            }
            catch(Exception e)
            {
                ret = 0;
            }

            return ret;
        }

    }
}
