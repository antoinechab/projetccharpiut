﻿using ModelDeDonnees;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer2.Query
{
    internal class AbscenceQuery
    {
        private MonContext _contexte;
        public AbscenceQuery(MonContext contexte)
        {
            _contexte = contexte;
        }

        public List<Absence> GetAbsences()
        {
            return _contexte.Absences.ToList();
        }

        public Absence GetAbscenceById(int id)
        {
            return _contexte.Absences.FirstOrDefault(e => e.Id == id);
        }

        public List<Absence> GetAbscenceByEleveId(int id)
        {
            return _contexte.Absences.ToList().FindAll(a => a.EleveId == id);
        }

        public int GetTotalAbscenceByEleveId(int id)
        {           
            return _contexte.Absences.ToList().FindAll(a => a.EleveId == id).Count;
        }

        public List<Absence> GetLastAbsence()
        {
            return _contexte.Absences.ToList().OrderByDescending(a => a.DateAbsence).Take(5).ToList();
        }

    }
}
