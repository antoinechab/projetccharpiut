﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ApplicationWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
               name: "createEleve",
               url: "Eleves/create",
               defaults: new { controller = "Eleves", action = "createEleve" }
           );

            routes.MapRoute(
               name: "DetailEleve",
               url: "Eleves/{action}/{id}",
               defaults: new { controller = "Eleves", action = "DetailELeve", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "DetailClasse",
               url: "Classe/{action}/{id}",
               defaults: new { controller = "Classe", action = "DetailClasse", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "DeleteNote",
               url: "Eleves/{action}/{id}",
               defaults: new { controller = "Eleves", action = "DeleteNote", id = UrlParameter.Optional }
           );
            
            routes.MapRoute(
               name: "DeleteClasse",
               url: "Classe/{action}/{id}",
               defaults: new { controller = "Classe", action = "DeleteClasse", id = UrlParameter.Optional }
           );
            
            routes.MapRoute(
               name: "DeleteAbsence",
               url: "Eleves/{action}/{id}",
               defaults: new { controller = "Eleves", action = "DeleteAbsence", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "ActionNote",
               url: "Eleves/{action}/{id}/{idEleve}",
               defaults: new { controller = "Eleves", action = "ActionNote", id = UrlParameter.Optional, idEleve = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "ActionAbsence",
               url: "Eleves/{action}/{id}/{idEleve}",
               defaults: new { controller = "Eleves", action = "ActionAbsence", id = UrlParameter.Optional, idEleve = UrlParameter.Optional }
           );
            
            routes.MapRoute(
               name: "ActionClasse",
               url: "Classe/{action}/{id}",
               defaults: new { controller = "Classe", action = "ActionClasse", id = UrlParameter.Optional, idEleve = UrlParameter.Optional }
           );
            
            routes.MapRoute(
               name: "createUpdateNote",
               url: "Eleves/note/{action}/{id}",
               defaults: new { controller = "Eleves", action = "createUpdateNote", id = UrlParameter.Optional }
           );
            
            routes.MapRoute(
               name: "createUpdateAbsence",
               url: "Eleves/abscence/{action}/{id}",
               defaults: new { controller = "Eleves", action = "createUpdateAbsence", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "updateEleve",
               url: "Eleves/update/{id}",
               defaults: new { controller = "Eleves", id = UrlParameter.Optional }
           );
            
            routes.MapRoute(
               name: "updateClasse",
               url: "Classe/update/{id}",
               defaults: new { controller = "Classe", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
