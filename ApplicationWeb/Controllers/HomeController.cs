﻿using ApplicationWeb.Models;
using BusinessLayer;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ApplicationWeb.Controllers
{
    public class HomeController : Controller
    {
        private BusinessManager _bm;
        public HomeController()
        {
            _bm = BusinessManager.GetInstance();
        }

        public ActionResult Index()
        {
            List<Eleve> eleves = _bm.GetBestStudent();
            List<ElevesViewModel> elevesViewModels = new List<ElevesViewModel>();
            foreach (var eleve in eleves)
            {
                elevesViewModels.Add(new ElevesViewModel
                {
                    id = eleve.Id,
                    nom = eleve.nom,
                    prenom = eleve.prenom,
                    dateNaissance = eleve.dateNaissance
                });
            }

            List<Absence> absences = _bm.GetLastAbsence();
            List<AbsenceViewModel> absenceViewModels = new List<AbsenceViewModel>();
            foreach (var absence in absences)
            {
                absenceViewModels.Add(new AbsenceViewModel
                {
                    Id = absence.Id,
                    Motif = absence.Motif,
                    DateAbsence = absence.DateAbsence,
                    EleveId = absence.EleveId
    
                });
            }

            AccueilViewModel accueil = new AccueilViewModel
            {
                bestEleves = elevesViewModels,
                lastAbscence = absenceViewModels
            };

            return View(accueil);
        }

        public ActionResult Classes()
        {
            List<Classe> classes = _bm.GetClasses();
            List<ClasseViewModel> classeViewModels = new List<ClasseViewModel>();
            foreach (var classe in classes)
            {
                classeViewModels.Add(new ClasseViewModel
                {
                    id = classe.Id,
                    Niveau = classe.Niveau,
                    NomEtablissement = classe.NomEtablissement
                });
            }

            return View(classeViewModels);
        }

        public ActionResult Eleves()
        {

            List<Eleve> eleves = _bm.GetEleves();
            List<ElevesViewModel> elevesViewModels = new List<ElevesViewModel>();
            foreach(var eleve in eleves)
            {
                elevesViewModels.Add(new ElevesViewModel { 
                    id = eleve.Id,
                    nom = eleve.nom,
                    prenom = eleve.prenom,
                    dateNaissance = eleve.dateNaissance
                });
            }

            return View(elevesViewModels);
        }
    }
}