﻿using ApplicationWeb.Models;
using BusinessLayer;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ApplicationWeb.Controllers
{
    public class ClasseController : Controller
    {

        private BusinessManager _bm;
        public ClasseController()
        {
            _bm = BusinessManager.GetInstance();
        }

        public ActionResult DetailClasse(int id)
        {
            Classe classe = _bm.GetClasseById(id);

            List<Eleve> eleves = _bm.GetElevesByClasseId(classe.Id);
            List<ElevesViewModel> elevesViewModels = new List<ElevesViewModel>();
            foreach (var eleve in eleves)
            {
                elevesViewModels.Add(new ElevesViewModel
                {
                    id = eleve.Id,
                    nom = eleve.nom,
                    prenom = eleve.prenom,
                    dateNaissance = eleve.dateNaissance
                });
            }

            ClasseViewModel classeViewModel = new ClasseViewModel
            {
                id = classe.Id,
                Niveau = classe.Niveau,
                NomEtablissement = classe.NomEtablissement,
                eleves = elevesViewModels
            };

            return View(classeViewModel ?? null);
        }

        public ActionResult DeleteClasse(int id)
        {

            return Content("todo");
        }

        public ActionResult ActionClasse(int id)
        {
            Classe classe = _bm.GetClasseById(id);

            ElevesViewModel elevesViewModel = new ElevesViewModel
            {
                classeId = classe.Id,
                dateNaissance = DateTime.Now
            };

            return View(elevesViewModel);
        }

        public ActionResult updateClasse()
        {


            string nom = Request.Params["nom"];
            string niveau = Request.Params["niveau"];
            int classeId = int.Parse(Request.Params["idClasse"]);

            Classe classe = _bm.GetClasseById(classeId);

            string retour;
            try
            {
                _bm.UpdateClasseById(classe.Id, nom, niveau);
                retour = "ok";
            }
            catch (Exception e)
            {
                retour = e.Message;
            }

            return Content(retour);
        }
    }
}