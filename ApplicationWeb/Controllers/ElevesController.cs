﻿using ApplicationWeb.Models;
using BusinessLayer;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace ApplicationWeb.Controllers
{
    public class ElevesController : Controller
    {
        private BusinessManager _bm;
        public ElevesController()
        {
            _bm = BusinessManager.GetInstance();
        }

        public ActionResult DetailELeve(int id)
        {
            Eleve eleve = _bm.GetEleveById(id);

            List<Notes> notes = _bm.GetNotesByEleveId(eleve.Id);
            List<NoteViewModel> noteViewModels = new List<NoteViewModel>();
            foreach (var note in notes)
            {
                noteViewModels.Add(new NoteViewModel
                {
                    Id = note.Id,
                    Appreciation = note.Appreciation,
                    DateNote = note.DateNote,
                    Matiere = note.Matiere,
                    Note = note.Note
                });
            }

            List<Absence> absences = _bm.GetAbscenceByEleveId(eleve.Id);
            List<AbsenceViewModel> absenceViewModels = new List<AbsenceViewModel>();
            foreach (var absence in absences)
            {
                absenceViewModels.Add(new AbsenceViewModel
                {
                    Id = absence.Id,
                    DateAbsence = absence.DateAbsence,
                    Motif = absence.Motif
                });
            }

            ElevesViewModel elevesViewModel = new ElevesViewModel
            {
                id = eleve.Id,
                nom = eleve.nom,
                prenom = eleve.prenom,
                dateNaissance = eleve.dateNaissance,
                classeId = _bm.GetClasseById(eleve.classeId).Id,
                notes = noteViewModels,
                absences = absenceViewModels,
                moyenne = _bm.GetAverageByEleveId(eleve.Id)
            };

            return View(elevesViewModel);
        }
        
        public ActionResult DeleteNote(int id)
        {
            string reponse;
            try
            {
                _bm.DeleteNoteById(id);
                reponse = "ok";
            }
            catch (Exception e)
            {
                reponse = e.Message;
            }

            return Content(reponse);
        }
        
        public ActionResult DeleteAbsence(int id)
        {
            string reponse;
            try
            {
                _bm.DeleteAbscenceById(id);
                reponse = "ok";
            }
            catch (Exception e)
            {
                reponse = e.Message;
            }

            return Content(reponse);
        }
        
        public ActionResult ActionNote(int id, int idEleve)
        {
            NoteViewModel noteViewModel;

            if (id == -1) 
            {
                noteViewModel = new NoteViewModel { EleveId = idEleve, DateNote = DateTime.Now };
            }
            else
            {
                Notes note = _bm.GetNoteById(id);

                noteViewModel = new NoteViewModel
                {
                    Id = note.Id,
                    Appreciation = note.Appreciation,
                    DateNote = note.DateNote,
                    Matiere = note.Matiere,
                    Note = note.Note,
                    EleveId = note.EleveId
                };
            }
          
            return View(noteViewModel);
        }
        
        public ActionResult createUpdateNote(int id)
        {
            string retour;

            Notes note = _bm.GetNoteById(id);

            string pattern = "dd/MM/yyyy";
            DateTime parsedDate;
            DateTime dateNote;

            String date = Request.Params["dateNote"];
            int idEleve = int.Parse(Request.Params["idEleve"]);
            string matiere = Request.Params["matiere"];
            int noteVal = int.Parse(Request.Params["note"]);
            string appreciation = Request.Params["appreciation"];

            if (DateTime.TryParseExact(date, pattern, null, DateTimeStyles.None, out parsedDate))
            {
                dateNote = parsedDate;
            }
            else
            {
                dateNote = DateTime.Now;
            }

            if (note == null)
            {
                try
                {
                    _bm.CreateNote(matiere, dateNote, appreciation, noteVal, idEleve);
                }
                catch(Exception e)
                {
                    retour = e.Message;
                }
                retour = "ok";
            }
            else
            {
                try
                {
                    _bm.UpdateNoteById(note.Id, matiere, dateNote, appreciation, noteVal, idEleve);
                }
                catch (Exception e)
                {
                    retour = e.Message;
                }
                retour = "ok";
            }
            
            return Content(retour) ;
        }
        
        public ActionResult createUpdateAbsence(int id)
        {
            string retour;

            Absence absence = _bm.GetAbscenceById(id);

            string pattern = "dd/MM/yyyy";
            DateTime parsedDate;
            DateTime dateNote;

            String date = Request.Params["date"];
            int idEleve = int.Parse(Request.Params["idEleve"]);
            string motif = Request.Params["motif"];

            if(DateTime.TryParseExact(date, pattern, null, DateTimeStyles.None, out parsedDate))
            {
                dateNote = parsedDate;
            }
            else
            {
                dateNote = DateTime.Now;
            }
            
            if (absence == null)
            {
                try
                {
                    _bm.CreateAbscence(dateNote,motif,idEleve);
                }
                catch(Exception e)
                {
                    retour = e.Message;
                }
                retour = "ok";
            }
            else
            {
                try
                {
                    _bm.UpdateAbscenceById(absence.Id, dateNote, motif, idEleve);
                }
                catch (Exception e)
                {
                    retour = e.Message;
                }
                retour = "ok";
            }
            
            return Content(retour) ;
        }
        
        public ActionResult ActionAbsence(int id, int idEleve)
        {
            AbsenceViewModel absenceViewModel;

            if (id == -1)
            {
                absenceViewModel = new AbsenceViewModel { EleveId = idEleve, DateAbsence = DateTime.Now };
            }
            else
            {
                Absence absence = _bm.GetAbscenceById(id);
                absenceViewModel = new AbsenceViewModel
                {
                    Id = absence.Id,
                    DateAbsence = absence.DateAbsence,
                    Motif = absence.Motif,
                    EleveId = absence.EleveId
                };
            }

            return View(absenceViewModel);
        }

        public ActionResult createEleve()
        {

            string pattern = "dd/MM/yyyy";
            DateTime parsedDate;
            DateTime dateNaissance;

            String date = Request.Params["dateNaissance"];
            string nom = Request.Params["nom"];
            string prenom = Request.Params["prenom"];
            int classeId = int.Parse(Request.Params["classeId"]);

            if (DateTime.TryParseExact(date, pattern, null, DateTimeStyles.None, out parsedDate))
            {
                dateNaissance = parsedDate;
            }
            else
            {
                dateNaissance = DateTime.Now;
            }

            string retour;
            try
            {
                _bm.CreateEleve(nom,prenom,dateNaissance,classeId);
                retour = "ok";
            }
            catch (Exception e)
            {
                retour = e.Message;
            }
           
            return Content(retour);
        }
        
        public ActionResult updateEleve()
        {

            string pattern = "dd/MM/yyyy";
            DateTime parsedDate;
            DateTime dateNaissance;

            String date = Request.Params["dateNaissance"];
            string nom = Request.Params["nom"];
            string prenom = Request.Params["prenom"];
            int idEleve = int.Parse(Request.Params["idEleve"]);
            int classeId = int.Parse(Request.Params["classeId"]);

            if (DateTime.TryParseExact(date, pattern, null, DateTimeStyles.None, out parsedDate))
            {
                dateNaissance = parsedDate;
            }
            else
            {
                dateNaissance = DateTime.Now;
            }

            Eleve eleve = _bm.GetEleveById(idEleve);

            string retour;
            try
            {
                _bm.UpdateEleveById(eleve.Id,nom,prenom,dateNaissance,classeId);
                retour = "ok";
            }
            catch (Exception e)
            {
                retour = e.Message;
            }
           
            return Content(retour);
        }
    }
}