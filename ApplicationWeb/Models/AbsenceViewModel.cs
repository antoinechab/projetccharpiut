﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationWeb.Models
{
    public class AbsenceViewModel
    {
        public int Id { get; set; }
        public DateTime DateAbsence { get; set; }
        public string Motif { get; set; }
        public int EleveId { get; set; }
    }
}