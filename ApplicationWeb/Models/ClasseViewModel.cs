﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationWeb.Models
{
    public class ClasseViewModel
    {
        public int id { get; set; }
        public string NomEtablissement { get; set; }
        public string Niveau { get; set; }
        public List<ElevesViewModel> eleves { get; set; }
    }
}