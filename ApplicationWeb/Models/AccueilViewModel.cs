﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationWeb.Models
{
    public class AccueilViewModel
    {
        public List<ElevesViewModel> bestEleves { get; set; }
        public List<AbsenceViewModel> lastAbscence { get; set; }
    }
}