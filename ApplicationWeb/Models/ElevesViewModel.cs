﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationWeb.Models
{
    public class ElevesViewModel
    {
        public int id { get; set; }
        public int classeId { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public DateTime dateNaissance { get; set; }
        public List<NoteViewModel> notes { get; set; }
        public List<AbsenceViewModel> absences { get; set; }
        public double moyenne { get; set; }
    }
}