﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationWeb.Models
{
    public class NoteViewModel
    {
        public int Id { get; set; }
        public string Matiere { get; set; }
        public DateTime DateNote { get; set; }
        public string Appreciation { get; set; }
        public int Note { get; set; }
        public int EleveId { get; set; }
    }
}