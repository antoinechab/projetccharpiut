﻿using ModelDeDonnees;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestModelDonnees
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MonContext monContext = new MonContext();
                List<Classe> classes = monContext.Classes.ToList();
                classes.ForEach(c => Console.WriteLine(c.Id));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
