﻿using ApplicationWPF.ViewModel.Common;
using BusinessLayer;
using ModelDeDonnees.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationWPF.ViewModel
{
    public class ListElevesViewModel : BaseViewModel
    {
        private ObservableCollection<DetailElevesViewModel> _eleves;
        private DetailElevesViewModel _selectedEleve;
        private BusinessManager _bm;
        public ListElevesViewModel()
        {
            _bm = BusinessManager.GetInstance();            
            _eleves = new ObservableCollection<DetailElevesViewModel>();

            foreach(Eleve eleve in _bm.GetEleves())
            {
                DetailElevesViewModel detailEleves = new DetailElevesViewModel 
                { 
                    Nom = eleve.nom,
                    Prenom = eleve.prenom,
                    DateNaissance = eleve.dateNaissance                    
                };

                detailEleves.Moyenne = _bm.GetAverageByEleveId(eleve.Id);
                detailEleves.Abscence = _bm.GetTotalAbscenceByEleveId(eleve.Id);

                _eleves.Add(detailEleves);
            }
            

            if (_eleves != null && _eleves.Count > 0)
            {
                _selectedEleve = _eleves.ElementAt(0);
            }
        }

        public ObservableCollection<DetailElevesViewModel> Eleves 
        { 
            get { return _eleves; }
            set { _eleves = value; OnPropertyChanged("Eleves"); }
        }
        public DetailElevesViewModel SelectedEleve 
        {
            get { return _selectedEleve; }
            set { _selectedEleve = value; OnPropertyChanged("SelectedEleve"); }
        }
    }
}
