﻿using ApplicationWPF.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ApplicationWPF.ViewModel
{
    public class DetailElevesViewModel : BaseViewModel
    {
       public string Nom { get; set; }
       public string Prenom { get; set; }
       public DateTime DateNaissance { get; set; }
       public double Moyenne { get; set; }
       public int Abscence { get; set; }

        //add other properties

        /*private void addNoteClick(object sender, RoutedEventArgs e)
        {

        }*/

        /*private void addAbscenceClick(object sender, RoutedEventArgs e)
        {
            
        }*/


    }
}
