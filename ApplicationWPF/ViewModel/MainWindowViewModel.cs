﻿using ApplicationWPF.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationWPF.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {
        private ListElevesViewModel _listElevesViewModel;
        #region Constructor
        public MainWindowViewModel()
        {
            _listElevesViewModel = new ListElevesViewModel();
        }
        #endregion
        #region Properties
        public ListElevesViewModel ListElevesViewModel
        {
            get { return _listElevesViewModel; }
            set { _listElevesViewModel = value; }
        }
        #endregion
    }
}
